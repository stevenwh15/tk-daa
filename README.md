## Tugas Akhir DAA 

# Analyzing Ford-Fulkerson and Goldberg-Tarjan Algorithm in Maximum Flow Problem 

**Members:**
* Erica Harlin - 1906351013
* Geoffrey Tyndall - 1906350704
* Steven Wiryadinata Halim - 1906350622

**Folders:**
* Algorithm: Code used in experiment
  * Ford-Fulkerson
  * Goldberg-Tarjan
* Tescase: Testcase used in experiment
  * testcase.zip: 100 testcase
  * generator: testcase generator
  * exampe: example testcase
* Plot: Code used to plot chart