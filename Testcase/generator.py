import sys
import time
from random import random, randint

sout = sys.stdout

class Edge:
    def __init__(self, source, sink, weight) -> None:
        self.source = source
        self.sink = sink
        self.weight = weight

    def __repr__(self) -> str:
        return f"{self.source} {self.sink} {self.weight}"

    @property
    def get_source(self):
        return self.source

    @property
    def get_sink(self):
        return self.sink


def check_edge_created(source, sink, edge_created):
    created = False

    for x in edge_created:
        if x.get_source == source:
            if x.get_sink == sink:
                created = True

    return created


def create_edge(n, edge_created):

    source = randint(1, n)
    sink = randint(1, n)
    while source == sink:
        sink = randint(1, n)

    if not check_edge_created(source, sink, edge_created):
        weight = randint(0, 100)
        new_edge = Edge(source, sink, weight)
        edge_created.append(new_edge)


def setup(no_tc):
    edge_created = []

    # Banyak verteks pada graf
    n = int(no_tc) + 2
    print(n)

    # Inisialisasi verteks
    for i in range(1, n + 1):
        print(i)

    # Banyak edge pada graf
    edge = randint(n + randint(0, 5), 50 * n)

    # Inisialisasi edge pada graf
    for i in range(edge):
        create_edge(n, edge_created)

    print(len(edge_created))

    for edge in edge_created:
        print(edge)

    # Penentuan source dan sink
    source = randint(1, n)
    sink = randint(1, n)
    while source == sink:
        sink = randint(1, n)
    print(f"{source} {sink}")


def generate(FILENAME):
    print(f"Generating TC{FILENAME}")
    FILENAME_IN = FILENAME + ".txt"

    original_stdout = sys.stdout  # Save a reference to the original standard output

    with open(FILENAME_IN, "w") as f:
        start = time.time()
        sys.stdout = f  # Change the standard output to the file we created.

        setup(FILENAME)

        sys.stdout = original_stdout  # Reset the standard output to its original value
        end = time.time()
        print("Input generation took", end - start, "seconds!")

    print("=" * 15)


banyak_tc = 5
for i in range(1, banyak_tc + 1):
    print(generate(str(i)))
